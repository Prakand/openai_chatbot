from django.contrib import messages
from django.shortcuts import render, redirect
import openai
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm


def home(request):
    # API =
    lang_list = ['c', 'clike', 'cpp', 'csharp', 'css', 'django', 'git', 'go', 'html', 'java', 'javascript', 'markup',
                 'markup-templating', 'matlab', 'mongodb', 'perl', 'php', 'python', 'regex', 'ruby', 'rust', 'sql']

    if request.method == 'POST':
        code = request.POST['code']
        lang = request.POST['lang']
        if lang == "Select Programming Language":
            messages.success(request, "Hey, You Forgot to select programming Language")
            return render(request, 'home.html', {'lang_list': lang_list, 'code': code, 'lang': lang})
        else:

            # OPENAI
            openai.api_key = "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"
            openai.Model.list()

            try:
                response = openai.Completion.create(
                    engine="text-davinci-003",
                    prompt=f"Respond only with code.Fix this {lang} code:{code}",
                    temperature=0,
                    max_tokens=1000,
                    top_p=1.0,
                    frequency_penalty=0.0,
                    presence_penalty=0.0,

                )
                response = (response["choices"][0]['text']).strip()
                return render(request, 'home.html', {'lang_list': lang_list, 'response': response, 'lang': lang})
            except Exception as e:
                return render(request, 'home.html', {'lang_list': lang_list, 'response': e, 'lang': lang})

    return render(request, 'home.html', {'lang_list': lang_list})


def suggest(request):
    lang_list = ['c', 'clike', 'cpp', 'csharp', 'css', 'django', 'git', 'go', 'html', 'java', 'javascript', 'markup',
                 'markup-templating', 'matlab', 'mongodb', 'perl', 'php', 'python', 'regex', 'ruby', 'rust', 'sql']

    if request.method == 'POST':
        code = request.POST['code']
        lang = request.POST['lang']
        if lang == "Select Programming Language":
            messages.success(request, "Hey, You Forgot to select programming Language")
            return render(request, 'suggest.html', {'lang_list': lang_list, 'code': code, 'lang': lang})
        else:

            # OPENAI
            openai.api_key = "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"
            openai.Model.list()

            try:
                response = openai.Completion.create(
                    engine="text-davinci-003",
                    prompt=f"Respond only with code.{code}",
                    temperature=0,
                    max_tokens=1000,
                    top_p=1.0,
                    frequency_penalty=0.0,
                    presence_penalty=0.0,

                )
                response = (response["choices"][0]['text']).strip()
                return render(request, 'suggest.html', {'lang_list': lang_list, 'response': response, 'lang': lang})
            except Exception as e:
                return render(request, 'suggest.html', {'lang_list': lang_list, 'response': e, 'lang': lang})
    return render(request, 'suggest.html', {'lang_list': lang_list})


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, "You have been loged in")
            return redirect('home')
        else:
            messages.success(request, "Error Logging In, Please enter valid details")
            return redirect('home')
    return render(request, 'home.html', {})


def logout_user(request):
    logout(request)
    messages.success(request, "You have been logged out.")
    return redirect('home')


def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, "You have been registered successfully.")
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'register.html',{'form':form})
